package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {

    void createPost(String stringToken, Post post);
    // Viewing all posts
    Iterable<Post> getPosts();
    // Delete a post
    ResponseEntity deletePost(String stringToken,Long id);

    //Update a post
    ResponseEntity updatePost(Long id,String stringToken, Post post);


//  to get all post of a specific user

    Iterable<Post> getMyPosts(String stringToken);


}
